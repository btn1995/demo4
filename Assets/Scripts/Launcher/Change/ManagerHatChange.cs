using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerHatChange : MonoBehaviour
{
    private ManagerHat mngHat;
    public Transform head;
    private int indexPre;
    // Start is called before the first frame update
    void Start()
    {
        mngHat = GameObject.Find("ManagerHat").GetComponent<ManagerHat>();
        GameObject defaultHat = mngHat.hats[0];
        Instantiate(defaultHat, head);
        indexPre = 0;
    }

    public void ChangeHat(int hatIndex)
    {
        if (hatIndex != indexPre)
        {
            Destroy(head.GetChild(0).gameObject);
            GameObject tempHat = mngHat.hats[hatIndex];
            Instantiate(tempHat, head);
            indexPre = hatIndex;
        }
        
    }
}
