using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerWeaponChange : MonoBehaviour
{
    private ManagerWeapon mngWeapon;
    public Transform handR;
    private int indexPre;

    // Start is called before the first frame update
    void Start()
    {
        mngWeapon = GameObject.Find("ManagerWeapon").GetComponent<ManagerWeapon>();
        GameObject defaultWeapon = mngWeapon.weapons[0];
        Instantiate(defaultWeapon, handR);
        indexPre = 0;
    }

    public void ChangeWeapon(int weaponIndex)
    {
        if (weaponIndex != indexPre)
        {
            Destroy(handR.GetChild(0).gameObject);
            GameObject tempWeapon = mngWeapon.weapons[weaponIndex];
            Instantiate(tempWeapon, handR);
            indexPre = weaponIndex;
        }
    }
}
