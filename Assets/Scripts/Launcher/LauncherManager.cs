using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class LauncherManager : MonoBehaviour
{
    //UI
    public InputField nameIF;
    //Character
    public GameObject[] characters;
    private string name;
    //Change Hats
    public ManagerHatChange mngHatChange;
    //Change Weapons
    public ManagerWeaponChange mngWeaponChange;

    //Change Setting
    public GameObject[] viewSetting;
    //Coin
    private int totalCoin;

    public static bool isStartGame;
    private void Start()
    {
        characters[0].SetActive(false);
        characters[1].SetActive(false);
        characters[2].SetActive(false);
        //
        mngHatChange = GameObject.FindGameObjectWithTag("Player").GetComponent<ManagerHatChange>();
        //
        mngWeaponChange = GameObject.FindGameObjectWithTag("Player").GetComponent<ManagerWeaponChange>();
        //default ViewSetting
        viewSetting[0].SetActive(true);
        //
        totalCoin = 0;
        //
        isStartGame = false;
    }
    void Update()
    {
        name = nameIF.text;
        if (isStartGame==false)
            totalCoin += SC_IRPlayer.coinCount;
    }
    public void onStartGame()
    {
        if (name != null)
        {
            SceneManager.LoadScene("PlayScene");
            isStartGame = true;
        }   
    }
    public void onCharacterSetting()
    {
        viewSetting[0].SetActive(true);
        viewSetting[1].SetActive(false);
        viewSetting[2].SetActive(false);
        viewSetting[3].SetActive(false);
    }
    public void onClothesSetting()
    {
        viewSetting[0].SetActive(false);
        viewSetting[1].SetActive(true);
        viewSetting[2].SetActive(false);
        viewSetting[3].SetActive(false);
    }
    public void onHatSetting()
    {
        viewSetting[0].SetActive(false);
        viewSetting[1].SetActive(false);
        viewSetting[2].SetActive(true);
        viewSetting[3].SetActive(false);
    }
    public void onWeaponSetting()
    {
        viewSetting[0].SetActive(false);
        viewSetting[1].SetActive(false);
        viewSetting[2].SetActive(false);
        viewSetting[3].SetActive(true);
    }
    //Change Character
    public void SelectMale()
    {
        characters[0].SetActive(true);
        characters[1].SetActive(false);
        characters[2].SetActive(false);
    }
    public void SelectFemale()
    {
        characters[0].SetActive(false);
        characters[1].SetActive(true);
        characters[2].SetActive(false);
    }
    public void SelectPolice()
    {
        characters[0].SetActive(false);
        characters[1].SetActive(false);
        characters[2].SetActive(true);
    }

    //Change Item
    public void ButtonHatChange()
    {
        GameObject tempBtn = EventSystem.current.currentSelectedGameObject;
        int tempBtnIndex = tempBtn.transform.GetSiblingIndex();

        mngHatChange.ChangeHat(tempBtnIndex);
        //Debug.Log("Hat : " + tempBtnIndex);
    }
    public void ButtonWeaponChange()
    {
        GameObject tempBtn = EventSystem.current.currentSelectedGameObject;
        int tempBtnIndex = tempBtn.transform.GetSiblingIndex();

        mngWeaponChange.ChangeWeapon(tempBtnIndex);
    }
}
