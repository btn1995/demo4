using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_IRPlayer : MonoBehaviour
{
    public CharacterController controller;

    public float gravity = -10f;
    public float jumpHeight = 2f;
    private Vector3 playerVelocity;
    //Move
    private int desireLane=1;
    private float laneDistance=4f;
    //Put Item
    public bool isItem;
    float countTime;
    //Virus effect
    public GameObject effectGO;
    private bool isEffect;
    float countTimeE;

    bool grounded = false;

    bool crouch = false;

    //
    public int vacinCount;
    public static int coinCount;
    //Magnet effect
    public GameObject coinDetector;
    public float magnetCount;

    // Start is called before the first frame update
    void Start()
    {
        //anim = GetComponent<Animator>();
        isItem = false;
        isEffect = false;
        vacinCount = 0;
        coinCount = 0;
        magnetCount = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        //anim.SetBool("isRunning", true);
        //grounded = controller.isGrounded;
        Debug.Log(grounded);
        if (grounded && playerVelocity.y < 1.15f)
            playerVelocity.y = 1.15f;

        //Move
        //if (Input.GetKeyDown(KeyCode.LeftArrow))
        //{
        //    Debug.Log("left");
        //    desireLane--;
        //    if (desireLane == -1)
        //        desireLane = 0;
        //}
        //if (Input.GetKeyDown(KeyCode.RightArrow))
        //{
        //    desireLane++;
        //    if (desireLane == 3)
        //        desireLane = 2;
        //}
        //Vector3 targetPosition = transform.position.y * Vector3.up;
        //if (desireLane == 0)
        //{
        //    targetPosition += Vector3.left * laneDistance;
        //}
        //else if (desireLane == 2)
        //{
        //    targetPosition += Vector3.right * laneDistance;
        //}
        //transform.position = Vector3.Lerp(transform.position, targetPosition, 80 * Time.deltaTime);
        float horizontal = Input.GetAxisRaw("Horizontal");
        Vector3 move = new Vector3(horizontal, 0, 0);
        controller.Move(move * Time.deltaTime * 30f);

        //Jump
        if (Input.GetKeyDown(KeyCode.W))
        {
            Debug.Log("Jump");
            if (isItem == true)
                playerVelocity.y += Mathf.Sqrt(-4 * jumpHeight * gravity);
            playerVelocity.y += Mathf.Sqrt(-2 * jumpHeight * gravity);
        }
        playerVelocity.y += gravity * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);

        //Crouch
        crouch = Input.GetKey(KeyCode.S);
        if (crouch)
        {
            transform.localRotation = Quaternion.Euler(-70, transform.localRotation.y, transform.localRotation.z);
            //transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(defaultScale.x, defaultScale.y * 0.4f, defaultScale.z), Time.deltaTime * 7);
        }
        else
        {
            //transform.localScale = Vector3.Lerp(transform.localScale, defaultScale, Time.deltaTime * 7);
            transform.localRotation = Quaternion.Euler(transform.localRotation.x, transform.localRotation.y, transform.localRotation.z);
        }

        //time use item
        if (countTime > 0)
            countTime -= Time.deltaTime;
        else
            isItem = false;
        //time effect Virus
        if (countTimeE > 0)
        {
            countTimeE -= Time.deltaTime;
        }
        else
        {
            isEffect = false;
            effectGO.SetActive(false);
        }
        if (magnetCount > 0)
            magnetCount -= Time.deltaTime;
        else
        {
            coinDetector.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Finish")
        {
            SC_GroundGenerator.instance.gameOver = true;
        }
        if (other.gameObject.tag == "Item")
        {
            isItem = true;
            countTime = 3;
        }
        if (other.gameObject.tag == "Ground")
            grounded = true;
        if (other.gameObject.tag == "Virus")
        {
            if (vacinCount > 0)
                vacinCount--;
            else
            {
                isEffect = true;
                countTimeE = 3;
                effectGO.SetActive(true);
            }
        }
        if (other.gameObject.tag == "Coin")
            coinCount++;
        if (other.gameObject.tag == "Vacine")
            vacinCount++;
        if (other.gameObject.tag == "Magnet")
        {
            magnetCount = 10;
            coinDetector.SetActive(true);
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Ground")
            grounded = false;
    }
}
