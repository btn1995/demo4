using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_PlatformTile : MonoBehaviour
{
    public Transform startPoint;
    public Transform endPoint;
    public GameObject[] obstacles;
    public void ActiveRandomObstacles()
    {
        DeactivateAllObstacles();

        //System.Random random = new System.Random();
        //int randomNumber = random.Next(0, obstacles.Length);
        int randomNumber = Random.Range(0, obstacles.Length);
        obstacles[randomNumber].SetActive(true);
    }
    public void DeactivateAllObstacles()
    {
        for (int i = 0; i < obstacles.Length; i++)
            obstacles[i].SetActive(false);
    }
}
